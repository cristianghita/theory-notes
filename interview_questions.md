# OOP

##   ArrayList vs LinkedList

**ArrayList**
- manipulation is **slow** because internally uses an array, if an element is removed all the bits are shifted in memory
- good for **storing and accessing data**

**LinkedList**
- manipulation is **faster** because it uses a doubly linked list, so no bit shifting
- good for **manipulating data**

<br />

##   Microservices vs Monolith

**Monolith**
- can evolve into a "big ball of mud"
- limited reuse
- scaling is difficult

**Microservices**
- services are encouraged to be small
- if microservices interfaces are exposed, they can be consumed and reused without direct coupling language buildings or shared libraries
- services exist as independent deployment and can be scaled independently

The tradeoff of microservices flexibility is **complexity**.


<br />

##   SOLID principles

<br/>

<u>**S**ingle-Responsability</u>
- A class should  have one and only one reason to change, meaning that a class hould have only one job.

<u>**O**pen-Closed</u>
- A class should be extendable without modifying the class iteself.

<u>**L**iskov Substitution</u>
- Every subclass or derived class should be substitutable for their base or parent class.

<u>**I**nterface Segregation</u>
- A client should not be forced to implement an interface that it doesn't use, nor clients shouldn't be forced to depend on methods they do not use.

<u>**D**ependency Inversion</u>
- Entities must depend on abstractions, not on concretions. It stated that the high-level module must not depend on the low-level module, but they should depend on abstractions.
  - This principle allows for decoupling

<br />
<br />



# React & NodeJs

##   How node.js works (event-loop)

**Phases:**
1. Timers
2. I/O Callbacks
3. Waiting / Preparation
4. I/O Polling
5. `setImmediate()` callbacks
6. Close Events

<br />

##   Explain all react hooks

<br/>

`useState()`

- handle reactive data
- any data that changes in the application is called state

<br/>

`useEffect()`
- allows us to implement all of the lifecycle hooks from within a function API

`React.useEffect(() => {})`

 will run when component **mounts and anytime stateful data changes**

`React.useEffect(() => {}, [])`

will run when the component **is first initialized**

`React.useEffect(() => {}, [count])`

will run only **when count state changes**

`React.useEffect(() => { return () => alert('bye'); })`

will run when the **component is destroyed or before it  is removed from UI**

<br />

`useContext()`
- allows us to work with **React's Context API** in order to pass props **avoiding prop-drilling**

<br/>

`useRef()`
- allows us to create a mutable object
- it is used like `useState()` but **it doesn't trigger a re-render when value changes**

<br/>

`useReducer()`
- manages state using **Redux Pattern**
- instead of updating the state, we **dispatch** actions

<br/>

`useMemo()`
- help optimise computational cost or improve performance
- mostly used when we're needed to make expensive calculations
- **memoizes variables**

<br/>

`useCallback()`
- **memoizes functions**

<br />

`useLayoutEffect()`
- it works the same as useEffect() hook with one difference - the callback will run **after rendering the component but before the actual updates have been painted to the screen**
- blocks visual updates until your callback is finished


<br />

##   Virtual DOM vs Normal DOM

<br/>

A virtual DOM object has the same properties as a real DOM object, but it lacks the real thing’s power to directly change what’s on the screen.

Manipulating the **DOM is slow**. Manipulating the **virtual DOM is much faster**, because nothing gets drawn onscreen. Think of manipulating the virtual DOM as editing a blueprint, as opposed to moving rooms in an actual house.

1. When you render a JSX element, every single virtual DOM object gets updated.

2. Once the virtual DOM has updated, then React compares the virtual DOM with a virtual DOM snapshot that was taken right before the update.

3. By comparing the new virtual DOM with a pre-update version, React figures out exactly which virtual DOM objects have changed. This process is called “diffing.”

4. Once React knows which virtual DOM objects have changed, then React updates those objects, and only those objects, on the real DOM.

<br />

##   Functional Component vs Class Component

<br/>

### **Functional Component** (_stateless component_)

- simple js functions that return html UI
- accept data and display them in some form
- accepts properties (props) and returns html (JSX)
- ? gives solution without using state
- no render method

**Lifecycle** - all lifecycles are replicated in `useEffects()`

### **Class Component** (_stateful component_)
- implement logic and state
- must have `render()` method
- complex UI Logic

**Lifecycle**
1. Mounting
2. Updating
3. Unmounting
4. Error Handeling

Why Functional over Class?
- easier to read & test
- less code
- easier to separate container and presentational components


<br />

##   Typescript over Javascript. Why?

<br/>

### Javascript Features

- Cross-platform language
- Used for client-side and server-side
- Dynamic & flexible

### Typescript Features

- Maintainability
- Code navigation & bug prevention
- Code 'discoverability' & rafactoring
- Optional Static Type Annotation
- Addition Features for Functions
- Supports ES6
- Support interfaces, sub-interfaces, classes and subclasses
- Scalable HTML5 client-side development
- Class-based object-oriented with the inheritance of private members and interfaces

<br />

##   How to reduce the output code to the browser for faster response

<br />
<br />


# Database

##   MongoDb joins

</br>

```
{
  $lookup:
  {
    from: <collection to join>,
    localField: <field from the input documents>,
    foreignField: <field from the documents of the "from" collection>,
    as: <output array field>
  }
}
```

<br />

##   SQL vs NoSQL

<br />

### **SQL**
- relational
- uses **structured query language**
- **predefined schema**
- **vertically scalable**
- table based
- **better for multi-row** transactions

### **NoSQL**
- non-relational
- **unstructured data**
- **dynamic schema**
- **horizontally scalable**
- document / key-value / graph / wide-column stores based
- **better for unstructured data** like documents or JSON


<br/>

##   Firebase Analytics & Notifications

<br />
<br />



# General

##   How scalability works
<br/>

### **What is scalability?**

Scalability is the capacity of a product, company, system, etc. to provide services that match growing demand.

<br/>

### **Vertical Scaling** (scaling up)
- adding more power (e.g: CPU, RAM) to an existing machine

### **Horizontal Scaling** (scaling out)
- adding more machines to your pool

<br />

##   How to handle a new task (workflow)

<br />

##   How to make your code easy to read/understand?
<br />

1. Prioritze

## Typescript closures

**Closures** is a term used to describe the concept that **lexical execution contexts contain references to their parent environments**.

In short, **closures describe persistent scope, and the ability of inner code blocks to reference outer variables**.

``` typescript
function createCounter() {
  let val = 0;
  return {
    increment() { val++ },
    getVal() { return val }
  }
}

let counter = createCounter();
counter.increment();
console.log(counter.getVal()); // 1
counter.increment();
console.log(counter.getVal()); // 2
```

## Access modifiers
<br/>

### `public`
By default, all members of a class in TypeScript are public. All the public members can be accessed anywhere without any restrictions.

<br />

### `private`
The private access modifier ensures that class members are visible only to that class and are not accessible outside the containing class.

<br />

### `protected`
The protected access modifier is similar to the private access modifier, except that protected members can be accessed using their deriving classes.

<br />


## Generics

## Collections (Sets)