# Memory Mangement

## Definitions

**memory leaks** - When used memory is never freed up.

**wild/dangling pointers** - An object is deleted but the pointer is reused.

**stack** - contains primitive types and references to objects

**heap** - stores reference types, like objects, strings or closures

<br />

## Garbage Collector
- prevents wild/dangling pointers
- won't free space that was freed up already
- will protect you from some types of memory leaks

<br />

## Tools
- profiler (e.g: [v8-profiler](https://github.com/node-inspector/v8-profiler), [node-inspector](https://github.com/node-inspector), [node-microtime](https://github.com/wadey/node-microtime))
- Google Chrome DevTools profiler

<br/>

## Memory Leaks Causes
- **Global variables:**  Since global variables in JavaScript are referenced by the root node (window or global this), they are never garbage collected throughout the lifetime of the application, and will occupy memory as long as the application is running.
- **Multiple references:** When the same object is referenced from multiple objects, it might lead to a memory leak when one of the references is left dangling.
- **Timers & Events:** The use of setTimeout, setInterval, Observers and event listeners can cause memory leaks when heavy object references are kept in their callbacks without proper handling.

## Best Practices to Avoid Memory Leaks
- REDUCE USE OF GLOBAL VARIABLES
  - Avoid Accidental Globals
- USE STACK MEMORY EFFECTIVELY
- USE HEAP MEMORY EFFECTIVELY
- PROPERLY USING CLOSURES, TIMERS AND EVENT HANDLERS

<br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/>

### Resources:
- https://blog.risingstack.com/node-js-at-scale-node-js-garbage-collection/#:~:text=Memory%20Management%20in%20Node.,-js%20Applications&text=Memory%20management%20provides%20ways%20to,that%20they%20can%20be%20reused.&text=Memory%20leaks%20when%20the%20used%20memory%20space%20is%20never%20freed%20up.
- https://blog.appsignal.com/2020/05/06/avoiding-memory-leaks-in-nodejs-best-practices-for-performance.html