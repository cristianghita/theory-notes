# Multithreading

### **Node.js** - a single-threaded asynchronous event-driven JavaScript runtime.

<br/>

## Definitions
- **Multithreading:** A single CPU core can handle multiple threads of execution concurrently.
- **Asynchrony:** Make events run separately form the application's primary thread and notify it via signals when an event is completed or failed.

<img src="https://miro.medium.com/max/645/1*G6mHZlqhKJ5_UxUWoCdUng.png" style="width:400px">

<br/>

## Node.js Threads
- **Event Loop** (aka the main thread)
  - Node.js runs JavaScript code in the Event Loop (initialization and callbacks) which is also responsible for fulfilling non-blocking asynchronous requests like network I/O.
- **Worker Pool** (aka threadpool)
  - As for Worker Pool threads which are responsible for offloading work for I/O APIs that can’t be done asynchronously at the OS level, as well as some particularly CPU-intensive APIs.

Performing the CPU-intensive synchronous tasks in worker threads and delegating only the I/O-intensive asynchronous tasks to the event-loop can dramatically improve the performance of our Node.js applications.

<img src="https://miro.medium.com/max/700/1*evBc40XtM4fshp1hQflL1A.png" style="width: 400px">


<br/>

## Is Node.js single-threaded?
Actually, we can run things in parallel, but we don’t create threads, and we don’t sync them. The virtual machine and the operating system run the I/O in parallel for us, and when it’s time to send data back to our JavaScript code, the JavaScript part is the one that runs in a single thread.

In other words, everything runs in parallel except for our JavaScript code. Synchronous blocks of JavaScript code are always run one at a time.

<br/>

## **Event Loop Phases:**
1. Timers
2. I/O Callbacks
3. Waiting / Preparation
4. I/O Polling
5. `setImmediate()` callbacks
6. Close Events




<br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/>

### Resources:
- https://blog.logrocket.com/node-js-multithreading-what-are-worker-threads-and-why-do-they-matter-48ab102f8b10/