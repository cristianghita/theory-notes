## Declare Variable

``` typescript
let employeeName: string = "Joe";
```
<br/>

## Data Type - Never
The never type is used when you are sure that something is never going to occur. For example, you write a function which will not return to its end point or always throws an exception.

``` typescript
function keepProcessing(): never { 
  while (true) { 
    console.log('I always does something and never ends.');
  }
}
```

<br/>

## Type Assertion

``` typescript
let code: any = 123;
let employeeCode = <number> code;
```

or

``` typescript
let code: any = 123;
let employeeCode = code as number;
```

<br/>

## Ternery Operator


``` typescript
x > y ? functionA() : functionB();
```

<br/>

## Switch

``` typescript
let day: number = 4;
switch(day) {
  case 0: {
    console.log("It's Monday.");
    break;
  }
  case 1: {
    console.log("It's Tuesday.");
    break;
  }
  ...
  default: {
    console.log("No such day exists!");
    break;
  }
}
```

<br/>

## For Loop

``` typescript
for (let i = 0; i < 3; i++) {
  console.log('Block statement exection no.' + i);
}

for (var char of str) {
  console.log(char);
}
```

Another form of the for loop is `for...in`. This can be used with an array, list, or tuple. The `for...in` loop iterates through a list or collection and returns an index on each iteration.

``` typescript
let arr = [10, 20, 30, 40];
for (var index in arr) {
  console.log(index); // prints indexes: 0, 1, 2, 3
  console.log(arr[index]); // prints elements: 10, 20, 30, 40
}
```


<br/>

## Function

### Arrow Function

``` typescript
let sum = (x: number, y: number): number => {
  return x + y;
}
```

if only 1 statement then:
``` typescript
let sum = (x: number, y: number) => x + y;
```

<br />

### Function Overloading

You can have multiple functions with the same name but **different parameter types and return type**. However, **the number of parameters should be the same**.

**OK**
``` typescript
function add(a:string, b:string): string;
function add(a:number, b:number): number;
function add(a: any, b:any): any {
  return a + b;
}
```

**NOT OK**
``` typescript
function display(a:string, b:string): void { //Compiler Error: Duplicate function implementation
  console.log(a + b);
}

function display(a:number): void { //Compiler Error: Duplicate function implementation
  console.log(a);
}
```

# Rest Parameters
When the number of parameters that a function will receive is not known or can vary, we can use rest parameters.

``` typescript
function Greet(greeting: string, ...names: string[]) {
  return greeting + " " + names.join(", ") + "!";
}
Greet("Hello", "Steve", "Bill"); // returns "Hello Steve, Bill!"
Greet("Hello");// returns "Hello !"
```
Remember, **rest parameters must come last in the function definition**, otherwise the TypeScript compiler will show an error.

<br/>

## Interface

### Interface as Type
``` typescript
interface KeyPair {
  key: number;
  value: string;
}
let kv1: KeyPair = { key:1, value:"Steve" }; // OK
let kv2: KeyPair = { key:1, val:"Steve" }; // Compiler Error: 'val' doesn't exist in type 'KeyPair'
```

### Interface as FunctionType
```typescript
interface KeyValueProcessor {
  (key: number, value: string): void;
};
function addKeyValue(key:number, value:string): void { 
  console.log(`addKeyValue: key = ${key}, value = ${value}`);
}
function updateKeyValue(key: number, value:string): void {
  console.log(`updateKeyValue: key = ${key}, value = ${value}`);
}   
let kvp: KeyValueProcessor = addKeyValue;
kvp(1, 'Bill'); //Output: addKeyValue: key = 1, value = Bill 
kvp = updateKeyValue;
kvp(2, 'Steve'); //Output: updateKeyValue: key = 2, value = Steve 
```

### Interface as ArrayType

``` typescript
interface IStringList {
  [index:string]: string;
}
let strArr : IStringList;
strArr["TS"] = "TypeScript";
strArr["JS"] = "JavaScript";
```

### Readonly Properties

``` typescript
interface Citizen {
  readonly SSN: number;
}
let personObj: Citizen  = { SSN: 110555444 }
personObj.SSN = '333666888'; // Compiler Error
```

### Extending Interfaces

``` typescript
interface IPerson {
  name: string;
  gender: string;
}
interface IEmployee extends IPerson {
  empCode: number;
}
let empObj:IEmployee = {
  empCode:1,
  name:"Bill",
  gender:"Male"
}
```

### Implementing an Interface

``` typescript
interface IEmployee {
    empCode: number;
    name: string;
    getSalary:(number)=>number;
}

class Employee implements IEmployee { 
  empCode: number;
  name: string;
  constructor(code: number, name: string) { 
              this.empCode = code;
              this.name = name;
  }
  getSalary(empCode:number):number { 
    return 20000;
  }
}
let emp = new Employee(1, "Steve");
```

## Typescript compiler converts .ts classes to .js classes using **closure**
<br/>

# Class

## Inheritance

``` ts
class Person {
  ...
}

class Employee extends Person {
  ...
}
```

## Implement Interfaces

``` ts
interface IPerson {
  ...
}
interface IEmployee {
  ...
}
class Employee implements IPerson, IEmployee {
  ...
}
```

## Method Overriding
```ts
class Car {
  ...
  run (speed: number = 0) {
    console.log(`This car is moving at ${speed} kmph!`);
  }
}
class Mercedes extends Car {
  ...
  run (speed = 150) {
    console.log('A mercedes started!');
    super.run(speed);
  }
}
```

## Abstract Class

Abstract classes can define `abstract functions` and `abstract properties`.

`Abstract properties` are used so they can be referenced in abstract classes without initialization.

`Abstract functions` are used so that child classes define their body.

```ts
abstract class Person {
  name: string;
  constructor(name: string) { this.name = name; }
  ...
  abstract find(string): Person;
}

class Employee extends Person {
  ...
  constructor(name: string, code: number) {
    super(name); // must call super()
    ...
  }

  find(name: string): Person {
    return new Employee(db.findName(name), db.findCode(name));
  }
}
```